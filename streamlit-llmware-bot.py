import os

# import streamlit modules
import streamlit as st

# import llmware modules
from llmware.library import Library
from llmware.retrieval import Query
from llmware.prompts import Prompt
from llmware.setup import Setup


# Configuration
DATA_PATH = '/home/linux-vm/llmware/Testing/Data/'

def create_vector_db():
    # input files and create vecotr db
    print (f"\n > Loading input files...")
    library = Library().create_new_library("data")
    library.add_files(DATA_PATH)

    # Create vector embeddings
    library.install_new_embedding(embedding_model_name='mini-lm-sbert',
                                  vector_db='milvus')
    return library

def do_semantic_search(library, query):
    os.environ["TOKENIZERS_PARALLELISM"] = "false"
    query_results = Query(library).semantic_query(query, result_count=20)
    return query_results

def load_llm():
    hf_model_list = ["llmware/bling-1b-0.1", "llmware/bling-1.4b-0.1", "llmware/bling-falcon-1b-0.1",
              "llmware/bling-sheared-llama-2.7b-0.1", "llmware/bling-sheared-llama-1.3b-0.1",
              "llmware/bling-red-pajamas-3b-0.1", "llmware/bling-stable-lm-3b-4e1t-0.1"]
    
    prompter = Prompt().load_model(hf_model_list[0],from_hf=True)
    return prompter

prompter = load_llm()

def get_response_from_llm(library, prompter, prompt_text):
    semantic_search_results = do_semantic_search(library, prompt_text)
    sources = prompter.add_source_query_results(semantic_search_results)
    responses = prompter.prompt_with_source(prompt_text)
    llm_response = responses["llm_response"]
    return llm_response

def initialize_session_state():
    if "history" not in st.session_state:
        st.session_state.history = []

def input_callback(user_input):
    st.session_state.history.append(user_input)

def answer_callback(answer):
    st.session_state.history.append(answer)

st.sidebar.header("Primary Details")
chat_bot_name = st.sidebar.text_input("Chatbot name")
chat_bot_description = st.sidebar.text_area("Description of your chatbot")

st.sidebar.header("Knowledge database")
uploaded_files = st.sidebar.file_uploader("Choose a pdf file", accept_multiple_files=True)

st.sidebar.header("Personalization")
temperature = st.sidebar.slider("Temperature", 0.1, 2.0, 1.0, 0.1)
max_length = st.sidebar.slider("Max Length", 10, 200, 50, 10)

train_bot_button = st.sidebar.button(label="Launch bot")

if train_bot_button:
    if chat_bot_name and chat_bot_description and uploaded_files and temperature and max_length:
        for uploaded_file in uploaded_files:
            with open(os.path.join(DATA_PATH, uploaded_file.name), "wb") as f:
                f.write(uploaded_file.getbuffer())

        library = create_vector_db()
    else:
        st.warning("Please fill all the fields")

input_text = st.chat_input("Say something")

initialize_session_state()

if input_text:
    answer = get_response_from_llm(library, prompter, input_text)
    input_callback(input_text)
    answer_callback(answer)

chat_placeholder = st.container()

with chat_placeholder:
    st.title(chat_bot_name)
    st.markdown(chat_bot_description)
    for i in range(0, len(st.session_state.history), 2):
        user_message = st.chat_message("user")
        user_message.write(st.session_state.history[i])
        bot_message = st.chat_message("assistant")
        bot_message.write(st.session_state.history[i + 1]["result"])
